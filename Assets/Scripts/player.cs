﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    [SerializeField] private TerrainGenerator terrainGenerator;

    private Animator animator;
    private bool isHopping;


    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    private void Update()
    {
        if (Input.GetKeyDown("w") && !isHopping)
        {
            Debug.Log("W");
            float zDifference = 0;
            if (transform.position.z % 1 != 0)
            {
                zDifference = Mathf.Round(transform.position.z) - transform.position.z;
                Debug.Log(transform.position);
                transform.Translate(new Vector3(1, 0, 0));
            }
              MoveCharacter(new Vector3(1, 0, zDifference));
        }
        if (Input.GetKeyDown("a") && !isHopping)
        {
            Debug.Log("A");
            MoveCharacter(new Vector3(0, 0, 1));
        }
        if (Input.GetKeyDown(KeyCode.D) && !isHopping)
        {
            Debug.Log("D");
            MoveCharacter(new Vector3(0, 0,-1));
        }

    }
    
    private void MoveCharacter(Vector3 difference)
    {
        animator.SetTrigger("hop");
        isHopping = true;
        transform.position = (transform.position + difference);
        terrainGenerator.SpawnTerrain(false, transform.position);
    }

    public void FinishHop()
    {
        isHopping = false;
    }
}
